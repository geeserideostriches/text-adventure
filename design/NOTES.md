# Text Adventure (engine?) project

## Features:

* Maps (title, description)
* Movement between maps

( Fill things out! )

Suggestions: Inventory, random monster system (do we want some sort of battle system?)?, NPC interactions? 

## Toolset:

* C++

## Data:

* Map files, containing data for...
  * title
  * description
  * neighbors
  * monsters(?)

## Design:

![Diagram of classes](schema.png)

schema.dia was created using the Dia diagram editor (https://sourceforge.net/projects/dia-installer/)

Example: 

* main() creates and starts Game.
* Game's Setup calls MapManager's Setup, which calls LoadData to load all the map data.
* Map Manager stores all the Rooms of the game and provides an "interface" that the Game State works with.

## Engine Features:

What we need to develop for the engine:

* Text User Interface (TUI)
  * Real console?
    * ncurses (not easy to set up on Windows, needs MSYS2)
    * Windows Console API
    * Just printing everything
  * Tiles?
    * Dwarf Fortress and libtcod use SDL and texture sheets to render ASCII grid
* Game file format + reader
  * Branching dialog paths
  * Maps
  * Event triggers
